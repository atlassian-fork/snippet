# -*- coding: utf-8 -*-
from pytest import raises

# The parametrize function is generated, so this doesn't work:
#
#     from pytest.mark import parametrize
#
import pytest

from snippet import metadata
from snippet.main import entry_point
from os import sys

parametrize = pytest.mark.parametrize


class TestMain(object):
    @parametrize('helparg', ['-h', '--help'])
    def test_help(self, helparg, capsys):
        with raises(SystemExit) as exc_info:
            sys.argv = ['snippet', helparg]
            entry_point()
        out, err = capsys.readouterr()
        # Should have printed some sort of usage message. We don't
        # need to explicitly test the content of the message.
        assert 'usage' in out
        # Should have used the program name from the argument
        # vector.
        assert 'snippet' in out
        # Should exit with zero return code.
        assert not exc_info.value.code

    @parametrize('versionarg', ['--version'])
    def test_version(self, versionarg, capsys):
        with raises(SystemExit) as exc_info:
            sys.argv = ['snippet', versionarg]
            entry_point()
        out, err = capsys.readouterr()
        # Should print out version.
        assert out == '{0} {1}\n'.format(metadata.project, metadata.version)
        # Should exit with zero return code.
        assert not exc_info.value.code == 0
