from builtins import input
from getpass import getpass
from os import path

from pybitbucket.bitbucket import Client
from pybitbucket.comment import Comment
from pybitbucket.commit import Commit
from pybitbucket.user import User
from pybitbucket.snippet import open_files, Snippet

from snippet import display
from snippet.utils import get_first, is_private
from snippet.config import CommandLineConfig


# It is sufficient to import Bitbucket types
# but pyflakes complains they are unused unless:
assert Snippet
assert Comment
assert Commit
assert User


def fail(args):
    display.show_command('fail')


def auth(args):
    params = {
        'set': args['--set'],
        'list': args['--list'],
    }
    display.show_command('auth', params)
    if (params['set'] or (
            not params['list'] and
            not path.isfile(CommandLineConfig.config_file()))):
        username = input('Username: ')
        password = getpass()
        email = input('Email: ')
        CommandLineConfig.save_config(username, password, email)

    all_cfgs = CommandLineConfig.load_all_configs()
    for url, values in all_cfgs.iteritems():
        display.show_config(url, values)


def list(args):
    bitbucket = Client(CommandLineConfig())
    role_switch = get_first(
        [k for (k, v) in args.iteritems()
         if (v and (k in ['--owner', '--contributor', '--member']))])
    role = role_switch.translate(None, '-') if role_switch else 'owner'
    display.show_command('list', 'role=%s' % role)
    for snip in Snippet.find_snippets_for_role(role, bitbucket):
        display.show_line(snip)


def create(args):
    params = {
        'title': args['--title'],
        'is_private': is_private(args['--public'], args['--private']),
        'files': args['FILES'],
    }
    bitbucket = Client(CommandLineConfig())
    display.show_command('create', params)
    snip = Snippet.create_snippet(
        files=open_files(params['files']),
        title=params['title'],
        is_private=params['is_private'],
        client=bitbucket)
    display.show_details(snip)


def modify(args):
    params = {
        'id': args['<id>'],
        'title': args['--title'],
        'is_private': is_private(args['--public'], args['--private']),
        'files': args['FILES'],
    }
    bitbucket = Client(CommandLineConfig())
    display.show_command('modify', params)
    snip = Snippet.find_my_snippet_by_id(params['id'], bitbucket)
    snip = snip.modify(
        files=open_files(params['files']),
        title=params['title'],
        is_private=params['is_private'])
    display.show_details(snip)


def delete(args):
    id = args['<id>']
    bitbucket = Client(CommandLineConfig())
    display.show_command('delete', 'id=%s' % id)
    snip = Snippet.find_my_snippet_by_id(id, bitbucket)
    display.show_line(snip)
    if snip:
        snip.delete()


def info(args):
    id = args['<id>']
    bitbucket = Client(CommandLineConfig())
    display.show_command('info', 'id=%s' % id)
    snip = Snippet.find_my_snippet_by_id(id, bitbucket)
    display.show_details(snip)


def files(args):
    id = args['<id>']
    bitbucket = Client(CommandLineConfig())
    display.show_command('files', 'id=%s' % id)
    snip = Snippet.find_my_snippet_by_id(id, bitbucket)
    for file in snip.files:
        display.show_filename(file)


def content(args):
    id = args['<id>']
    filename = args['<filename>']
    bitbucket = Client(CommandLineConfig())
    display.show_command('content', 'id=%s filename=%s' % (id, filename))
    snip = Snippet.find_my_snippet_by_id(id, bitbucket)
    c = snip.content(filename)
    display.show_content(c)


def watchers(args):
    id = args['<id>']
    bitbucket = Client(CommandLineConfig())
    display.show_command('watchers', 'id=%s' % id)
    snip = Snippet.find_my_snippet_by_id(id, bitbucket)
    for watcher in snip.watchers():
        display.show_user(watcher)


def comments(args):
    id = args['<id>']
    bitbucket = Client(CommandLineConfig())
    display.show_command('comments', 'id=%s' % id)
    snip = Snippet.find_my_snippet_by_id(id, bitbucket)
    for comment in snip.comments():
        display.show_comment(comment)


def commits(args):
    id = args['<id>']
    bitbucket = Client(CommandLineConfig())
    display.show_command('commits', 'id=%s' % id)
    snip = Snippet.find_my_snippet_by_id(id, bitbucket)
    for commit in snip.commits():
        display.show_commit(commit)


def commit(args):
    id = args['<id>']
    sha1 = args['<sha1>']
    display.show_command('commit', 'id=%s sha1=%s' % (id, sha1))
