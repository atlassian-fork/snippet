#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A command-line interface for creating, inspecting, and editing
Bitbucket Snippets.

usage:
    snippet (-h | --help)
    snippet auth [(--set|--list)]
    snippet list [--owner|--contributor|--member]
    snippet create [--title=<title>] [--public|--private] [FILES ...]
    snippet modify <id> [--title=<title>] [--public|--private] [FILES ...]
    snippet delete <id>
    snippet info <id>
    snippet files <id>
    snippet content <id> <filename>
    snippet watchers <id>
    snippet comments <id>
    snippet commits <id>
    snippet commit <id> <sha1>
    snippet --version
"""
from __future__ import print_function
from docopt import docopt

from pybitbucket.bitbucket import ServerError

from snippet import metadata
from snippet import commands
from snippet.utils import get_first
from snippet.display import show_server_error


def is_command(s):
    if s.startswith('-'):
        return False
    if s.startswith('<'):
        return False
    return True


def dispatch_command(arguments, command='fail'):
    f = getattr(commands, "{}".format(command), commands.fail)
    return f(arguments)


def main():
    version_string = '{0} {1}'.format(metadata.project, metadata.version)
    arguments = docopt(__doc__, version=version_string)
    command = get_first([
        k for (k, v) in arguments.iteritems()
        if (v and is_command(k))])
    try:
        dispatch_command(arguments, command)
    except ServerError as e:
        show_server_error(e)


def entry_point():
    """Zero-argument entry point for use with setuptools/distribute."""
    main()

if __name__ == "__main__":
    main()
